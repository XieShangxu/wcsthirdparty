var express = require('express');
var redis = require('redis');
var axios = require('axios');
var app = express();
var xmlParser = require('express-xml-bodyparser');
var WXBizMsgCrypt = require('./wxcrypto');

var ENV = require('./config/env').wechat_platform;

var redisClient = redis.createClient();

app.use(express.json());
app.use(xmlParser());

app.post('/weixin', function(req, res) {
  var msg = req.body.xml.encrypt[0];
  var cryptor = new WXBizMsgCrypt(ENV.token, ENV.encodingAESKey, ENV.AppID);
  var decryptedMsg = cryptor.decrypt(msg).message;
  var component_verify_ticket = '';
  decryptedMsg.replace(/<ComponentVerifyTicket><!\[CDATA\[ticket@@@(.*)\]\]><\/ComponentVerifyTicket>/ig, function(m1, m2) {
    component_verify_ticket = m2;
    return m1;
  });

  redisClient.set('component_verify_ticket', component_verify_ticket);
  res.send('success');
});

app.get('/list', function(req, res) {
  var authCode = req.query.auth_code;
  redisClient.get('component_access_token', function(err, token) {
    redisClient.get('authorizer_access_token_expired_at', function(err, expireTime) {
      if (!expireTime || (new Date() > new Date(expireTime))) {
        axios({
          method: 'post',
          url: 'https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=' + token,
          data: {
            component_appid: ENV.AppID,
            authorization_code: authCode
          }
        }).then(function(resp) {
          var data = resp.data;
          if (!data.errmsg) {
            var accessToken = data.authorization_info.authorizer_access_token;
            var refreshToken = data.authorization_info.authorizer_refresh_token;

            var expireDate = new Date();
            expireDate.setHours(expireDate.getHours() + 1);
            expireDate.setMinutes(expireDate.getMinutes() + 50);
            redisClient.set('authorizer_access_token_expired_at', expireDate.toString());
            redisClient.set('authorizer_access_token', accessToken);
            redisClient.set('authorizer_refresh_token', refreshToken);
            redisClient.set('authorizer_appid', data.authorization_info.authorizer_appid);

            getUserList(accessToken);
          }
        });
      } else {
        redisClient.get('authorizer_access_token', function(err, accessToken) {
          getUserList(accessToken);
        })
      }
    });
  })
  res.send('List Page');
})

app.get('/', function(req, res) {
  var component_verify_ticket = '';
  redisClient.get('component_verify_ticket', function(err, object) {
    component_verify_ticket = object;
    redisClient.get('access_token_expired_at', function(err, object) {
      if (!object || (new Date() > new Date(object))) {
        axios({
          method: 'post',
          url: 'https://api.weixin.qq.com/cgi-bin/component/api_component_token',
          data: {
            component_appid: ENV.AppID,
            component_appsecret: ENV.AppSecret,
            component_verify_ticket: component_verify_ticket
          }
        }).then(function(resp) {
          var data = resp.data;
          var expireDate = new Date();
          expireDate.setHours(expireDate.getHours() + 1);
          expireDate.setMinutes(expireDate.getMinutes() + 50);
          redisClient.set('access_token_expired_at', expireDate.toString());
          redisClient.set('component_access_token', data.component_access_token);
          getPreAuthCode(res, data.component_access_token);
        })
      } else {
        redisClient.get('component_access_token', function(err, token) {
          getPreAuthCode(res, token);
        })
      }
    })
  })
});


function getPreAuthCode(res, accessToken) {
  axios({
    method: 'post',
    url: 'https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=' + accessToken,
    data: {
      component_appid: ENV.AppID
    }
  }).then(function(resp) {
    var data = resp.data;
    if (!data.errmsg) {
      var preAuthCode = data.pre_auth_code.replace(/preauthcode@@@/ig, '');
      redisClient.set('pre_auth_code', preAuthCode);
      var redirectUri = encodeURIComponent('http://westore.danielxie.cn/list');
      res.send('<a href="https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=' + ENV.AppID + '&pre_auth_code=' + preAuthCode + '&redirect_uri=' + redirectUri + '">点击授权</a>');
    } else {
      res.send('Error');
    }
  })
}

function getUserList(accessToken, nextId) {
  axios({
    method: 'get',
    url: 'https://api.weixin.qq.com/cgi-bin/user/get?access_token=' + accessToken
  }).then(function(resp) {
    var data = resp.data;
    var openIDList = data.data.openid;

    for (var i = 0; i < openIDList.length; i++) {
      var openID = openIDList[i];
      getUserInfo(accessToken, openID);
    }
  });
}

function getUserInfo(accessToken, openId) {
  axios({
    method: 'get',
    url: 'https://api.weixin.qq.com/cgi-bin/user/info?access_token=' + accessToken + '&openid=' + openId + '&lang=zh_CN'
  }).then(function(resp) {
    var data = resp.data;
    console.log(data);
  });
}

app.listen(3131, function() {
  console.log('Listen: 3131');
})