var wechat = require('wechat');
var ENV = require('./config/env');

var config = {
  token: ENV.token,
  appid: ENV.AppID,
  encodingAESKey: ENV.encodingAESKey,
  checkSignature: false,
};

module.exports = {
  component_verify_ticket: wechat(config, function(req, res, next) {
    console.log('aaa');
    var message = req.weixin;
    if (message.InfoType === 'component_verify_ticket') {
      console.log('ComponentVerifyTicket', message.ComponentVerifyTicket);
    }
    res.send('success');
  })
}